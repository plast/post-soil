![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

---

Example plain HTML site using GitLab Pages.

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  

- [About the Post-soil Website](#About the Post-soil Website)

- [Whats Next?](#Whats Next?)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## About the Post-soil Website

The site was built with gitlab pages. https://gitlab.com/groups/pages Message me on the matrix server for any critques or suggestions. The site will never track you, always be open source, and will not run javascript. Feel free to use umatrix to verify this.

## Whats Next?

Some of the things in the future for post-soil are a peertube instance, irc server, and a master list of projects that respect the freedom of a node.










